// https://myrusakov.ru/js-drag-drop.html
var p = document.getElementsByTagName('p');
var choice = document.getElementsByClassName('choice');
var dragItem = null;
// обработчики событий для всех элементов
for(var i of p){
	i.addEventListener('dragstart', dragStart); // возникает в момент начала захвата элемента
	i.addEventListener('dragend', dragEnd); // возникает в момент отпускания элемента
}
// События для принимающего объекта
for (j of choice){
	j.addEventListener('dragenter', dragEnter); // наступает при заходе курсора перетаскиваемого элемента на принимающий блок
	j.addEventListener('dragover', dragOver); // наступает в процессе движения перемещаемого элемента внутри целевого
	j.addEventListener('dragleave', dragLeave); // наступает когда перетаскиваемый элемент покидает целевой блок
	j.addEventListener('drop', Drop); // наступает, когда мы отпускаем элемент при одном условии, 
									  // что у события dragover должен быть прописан метод preventDefault()
}

function dragStart(){
	dragItem = this;
	setTimeout(()=>this.style.display = 'none', 0);
}
function dragEnd(){
	dragItem = null;
	setTimeout(()=>this.style.display = 'block', 0);
}
function dragOver(e){
	e.preventDefault() // отменяет действие события по умолчанию
	this.style.border = "2px dotted red";
}
function dragEnter(e){
	e.preventDefault()
}
function dragLeave(){
	this.style.border = "none";
}
function Drop(){
	this.append(dragItem)
	this.style.border = "none";
}