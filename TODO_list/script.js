new Vue( {
  el: '#vue_module',
  data() {
    return {
      tasks: [],
      currentTask: { body: '', checked: false },
      tasksSet: new Set()
    }
  },
    methods: {
      addTask() {
        if (this.currentTask.body !== "") {
          if (!this.tasksSet.has(this.currentTask.body)) {
            this.tasksSet.add(this.currentTask.body)
            this.tasks.push(this.currentTask)
            this.currentTask = { body: "", checked: false }
          } else { alert('Tasks are repeated in the list'); }
        } else { alert('Please enter a non-empty string'); }
      },
      removeTask(index) {
        this.tasks.splice(index, 1)
      },
    }
  }
)